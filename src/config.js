import {IdentityServiceSdkConfig} from 'identity-service-sdk';
import {SpiffApiGatewayServiceSdkConfig} from 'spiff-api-gateway-service-sdk';
import {ClaimSpiffServiceSdkConfig} from 'claim-spiff-service-sdk';
import {PartnerRepServiceSdkConfig} from 'partner-rep-service-sdk';
import {AccountServiceSdkConfig} from 'account-service-sdk';
import {SessionManagerConfig} from 'session-manager';

export default class Config {

    _identityServiceSdkConfig:IdentityServiceSdkConfig;

    _spiffApiGatewaySdkConfig:SpiffApiGatewayServiceSdkConfig;

    _claimSpiffServiceSdkConfig:ClaimSpiffServiceSdkConfig;

    _partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig;

    _accountServiceSdkConfig:AccountServiceSdkConfig;

    _sessionManagerConfig:SessionManagerConfig;


    /**
     * @param {IdentityServiceSdkConfig} identityServiceSdkConfig
     * @param {SpiffApiGatewayServiceSdkConfig} spiffApiGatewayServiceSdkConfig
     * @param {ClaimSpiffServiceSdkConfig} claimSpiffServiceSdkConfig
     * @param {PartnerRepServiceSdkConfig} partnerRepServiceSdkConfig
     * @param {AccountServiceSdkConfig} accountServiceSdkConfig
     * @param {SessionManagerConfig} sessionManagerConfig

     */
    constructor(identityServiceSdkConfig:IdentityServiceSdkConfig,
                spiffApiGatewayServiceSdkConfig:SpiffApiGatewayServiceSdkConfig,
                claimSpiffServiceSdkConfig:ClaimSpiffServiceSdkConfig,
                partnerRepServiceSdkConfig:PartnerRepServiceSdkConfig,
                accountServiceSdkConfig:AccountServiceSdkConfig,
                sessionManagerConfig:SessionManagerConfig) {

        if (!identityServiceSdkConfig) {
            throw new TypeError('identityServiceSdkConfig required');
        }
        this._identityServiceSdkConfig = identityServiceSdkConfig;

        if (!spiffApiGatewayServiceSdkConfig) {
            throw new TypeError('spiffApiGatewayServiceSdkConfig required');
        }
        this._spiffApiGatewayServiceSdkConfig = spiffApiGatewayServiceSdkConfig;

        if (!claimSpiffServiceSdkConfig) {
            throw new TypeError('claimSpiffServiceSdkConfig required');
        }
        this._claimSpiffServiceSdkConfig = claimSpiffServiceSdkConfig;

        if (!partnerRepServiceSdkConfig) {
            throw new TypeError('partnerRepServiceSdkConfig required');
        }
        this._partnerRepServiceSdkConfig = partnerRepServiceSdkConfig;

        if (!accountServiceSdkConfig) {
            throw new TypeError('accountServiceSdkConfig required');
        }
        this._accountServiceSdkConfig = accountServiceSdkConfig;

        if (!sessionManagerConfig) {
            throw new TypeError('sessionManagerConfig');
        }
        this._sessionManagerConfig = sessionManagerConfig;

    }

    get identityServiceSdkConfig() {
        return this._identityServiceSdkConfig;
    }

    get spiffApiGatewayServiceSdkConfig() {
        return this._spiffApiGatewayServiceSdkConfig;
    }

    get claimSpiffServiceSdkConfig() {
        return this._claimSpiffServiceSdkConfig;
    }

    get partnerRepServiceSdkConfig() {
        return this._partnerRepServiceSdkConfig;
    }

    get accountServiceSdkConfig() {
        return this._accountServiceSdkConfig;
    }

    get sessionManagerConfig() {
        return this._sessionManagerConfig;
    }
}