import SpiffEntitlementTemplate from './templates/spiffEntitlements.html!text';
import SpiffEntitlementCntrl from './controllers/spiffEntitlementCntrl';
import SpiffClaimTemplate from './templates/submittedClaimSpiff.html!text';
import SpiffClaimCntrl from './controllers/submittedClaimsCntrl';

export default class RouteConfig {

    constructor($routeProvider) {

        $routeProvider
            .when('/',
                {
                    template:SpiffEntitlementTemplate,
                    controller: SpiffEntitlementCntrl,
                    controllerAs:'controller'
                }
            )
            .when('/spiffClaims',
                {
                    template:SpiffClaimTemplate,
                    controller: SpiffClaimCntrl,
                    controllerAs:'controller'
                }
            );

    }

}

RouteConfig.$inject = [
    '$routeProvider'
];