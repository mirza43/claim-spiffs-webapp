import './js/jquery-1.11.3.min';
import angular from 'angular';
import './js/angular-strap.min';
import './js/angular-strap.tpl.min';
import './js/ui-bootstrap-tpls';
import './bootstrap';
import 'header';
import 'footer';
import 'angular-route';
import ConfigFactory from './configFactory';
import Config from './config';
import configData from './configData.json!json';
import RouteConfig from './routeConfig';
import IdentityServiceSdk from 'identity-service-sdk';
import SpiffApiGatewayServiceSdk from 'spiff-api-gateway-service-sdk';
import ClaimSpiffServiceSdk from 'claim-spiff-service-sdk';
import SessionManager from 'session-manager';
import './js/bootstrap-select';
import 'style.css!css';
import './directives/selectDirective';
import ngFileUpload from 'danialfarid/ng-file-upload';
import PartnerRepServiceSdk from 'partner-rep-service-sdk';
import AccountServiceSdk from 'account-service-sdk';
import './directives/module';


angular
    .module(
        'claimSpiffWebApp.module',
        [
            'ngRoute',
            'ui.bootstrap',
            'mgcrea.ngStrap',
            'mgcrea.ngStrap.modal',
            'spiffEntitlement',
            'claimSpiff',
            'selectDirective',
            'header.module',
            'footer.module',
            'ngFileUpload',
            'country.module'
        ])
    .factory(
        'config',
        () => ConfigFactory.construct(configData)
    )
    .factory(
        'identityServiceSdk',
        [
            'config',
            config => new IdentityServiceSdk(config.identityServiceSdkConfig)
        ]
    )
    .factory(
        'spiffApiGatewayServiceSdk',
        [
            'config',
            config => new SpiffApiGatewayServiceSdk(config.spiffApiGatewayServiceSdkConfig)
        ]
    )
    .factory(
        'claimSpiffServiceSdk',
        [
            'config',
            config => new ClaimSpiffServiceSdk(config.claimSpiffServiceSdkConfig)
        ]
    )
    .factory(
        'partnerRepServiceSdk',
        [
            'config',
            config => new PartnerRepServiceSdk(config.partnerRepServiceSdkConfig)
    ]
    )
    .factory(
        'accountServiceSdk', [
            'config',
            config => new AccountServiceSdk(config.accountServiceSdkConfig)
        ]
    )
    .factory(
        'sessionManager',
        [
            'config',
            config => new SessionManager(config.sessionManagerConfig)
        ]
    ).config(['$routeProvider', $routeProvider => new RouteConfig($routeProvider)]);