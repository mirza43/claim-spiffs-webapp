import template from './country.html!text';
import AccountServiceSdk from 'account-service-sdk';
import IdentityServiceSdk from 'identity-service-sdk';
import SessionManager from 'session-manager';

export default class CountryController {

    constructor($uibModal,
                $scope,
                $location,
                $q,
                accountServiceSdk,
                identityServiceSdk:IdentityServiceSdk,
                sessionManager:SessionManager
            ) {

        this._sessionManager = sessionManager;

        $scope.countryFlag = function(){

            return $q((resolve,reject) =>{
                        sessionManager
                            .getAccessToken()
                            .then(accessToken => {
                                $q((resolve,reject) => {
                                    identityServiceSdk
                                        .getUserInfo(accessToken)
                                        .then(userInfo => {
                                            $q((resolve,reject) =>
                                                accountServiceSdk
                                                    .getCommercialAccountWithId(userInfo._account_id, accessToken)
                                                    .then(response => {
                                                        (`${response.address.countryIso31661Alpha2Code}` == 'US')|| (`${response.address.countryIso31661Alpha2Code}` == 'CA')? resolve(true) : reject(false);
                                                    })
                                            )
                                            .then(result => resolve(result))
                                            .catch(error => reject(false));
                                        });
                                    }
                                ).then((result)=>
                                    resolve(result)
                                ).catch(error=>reject(false));
                                }
                            );
                        })
        };

    }

}

CountryController.$inject = [
    '$uibModal',
    '$scope',
    '$location',
    '$q',
    'accountServiceSdk',
    'identityServiceSdk',
    'sessionManager'
];
