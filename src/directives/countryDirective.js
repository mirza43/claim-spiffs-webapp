import template from './country.html!text';
import 'bootstrap/css/bootstrap.css!css'
import 'bootstrap'
import './header.css!';
import CountryController from './controller'

function country($uibModal) {
    return {
        replace: false,
        controller: CountryController,
        controllerAs: 'controller',
        bindToController: true,
        link : function (scope,element,attributes) {
            scope.countryFlag()
                .then((result)=>{
                    console.log("Login In country matched?",result);
                    element.show();
                    scope.isValidCountry = true;
                })
                .catch(error=>{
                    console.log("Login In country matched?",error);
                    element.hide();
                    scope.isValidCountry = false;
                    $uibModal.open({
                        scope: scope,
                        template: template,
                        size: 'sm',
                        backdrop: 'static'
                    });
                });
        }
    }
}

export default country;